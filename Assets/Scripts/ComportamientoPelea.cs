﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoPelea : MonoBehaviour
{

    Personaje p1 = new Personaje("Ryu", 100, 20, 30);
    Personaje p2 = new Personaje("Ken", 100, 20, 30);

    void Start()
    {
       
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Debug.Log("Se presionó la tecla W");
            p1.pegar(p2);
            Debug.Log(p2.Nombre + "---" + p2.Vida + "HP");
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("Se presionó la tecla P");
            p2.pegar(p1);
            Debug.Log(p1.Nombre + "---" + p1.Vida + "HP");
        }

        if (p1.Vida ==0 || p2.Vida == 0)
        {
            Debug.Log("Game Over");
          
            Debug.Log(ganador(p1,p2)+" Ganó");
        }
    }
    string ganador(Personaje unpersonaje, Personaje otropersonaje)
    {
        string ganador;
        if (unpersonaje.Vida > otropersonaje.Vida)
        {
            ganador = unpersonaje.Nombre;
        }
        else
        {
            ganador = otropersonaje.Nombre;
        }
        return ganador;
    }
}

public class Personaje
{

    public string Nombre { get; set; }
    public int Vida { get; set; }
    public int Defensa { get; set; }
    public int Fuerza { get; set; }

    public Personaje (string nombre, int vida, int defensa, int fuerza)
    {
        Nombre = nombre;
        Vida = vida;
        Defensa = defensa;
        Fuerza = fuerza;
    }


    public override string ToString()
    {
        return base.ToString();
    }

    public void pegar (Personaje golpeado)
    {
        int daño = this.Fuerza - golpeado.Defensa;
        golpeado.Vida -= daño;
    }
}

/*
 * public class Arma
{
    public string NombreArma { get; set; }
    public int Capataque { get; set; }

    public Arma (string nombrearma, int capataque)
    {
        NombreArma = nombrearma;
        Capataque = capataque;
    }
}
*/