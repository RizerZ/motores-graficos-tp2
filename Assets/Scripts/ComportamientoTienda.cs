﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ComportamientoTienda : MonoBehaviour
{
    Arma[] _ = new Arma[10000];//creo un array de 10k de armas y reservo el espacio en memoria
    public Dictionary<string, int> dicarmas = new Dictionary<string, int>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 10000; i++)//lleno esos 10k de armas y los voy agregado 1 a 1 al diccionario
        {
            _[i]=new Arma(i);
            dicarmas.Add(_[i].NombreArma, _[i].Precio);
        }

        IOrderedEnumerable<KeyValuePair<string, int>> dicarmasordenado = dicarmas//codigo mágico encontrado por ahí que ordena todo
                       .OrderByDescending(x => x.Value);

        foreach (var item in dicarmasordenado)//muestro elemento por elemento del diccionario
        {
            Debug.Log(item.Key + " $" + item.Value);
        }
        int contador = 0;
        foreach (var item in dicarmasordenado)//cuento los elementos para asegurarme de que sean 10k
        {
            contador++;
        }
        Debug.Log(contador);
    }

}

    public class Arma
    {
        public string NombreArma { get; }
        public int Precio { get; }

        public Arma(int seed)//pido semilla para que cada elemento sea diferente
        {
        System.Random obj = new System.Random(seed);//uso la semilla
        string posibles = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int longitud = posibles.Length;
        char letra;
        int longitudnuevacadena = 10;
        string nuevacadena = "";

        for (int i = 0; i < longitudnuevacadena; i++)
        {
            letra = posibles[obj.Next(longitud)];
            nuevacadena += letra.ToString();
        }
        int R = obj.Next(999);//genero el precio

            NombreArma = nuevacadena;
            Precio = R;
        }
    } 


